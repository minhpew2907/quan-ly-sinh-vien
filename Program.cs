﻿
Student student = new Student("Nam", 15, 9);

Console.WriteLine($"Student grade before update: {student.GetInfo()}");

student.SetGrade(10);

Console.WriteLine($"Student grade after update: {student.GetInfo()}");