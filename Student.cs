﻿

class Student
{
    public string Name;
    public int Age;
    public int Grade;

    public Student(string name, int age, int grade)
    {
        Name = name;
        Age = age;
        Grade = grade;
    }

    public void SetGrade(int grade)
    {
        Grade = grade;
    }

    public string GetInfo()
    {
        return $"Name: {Name}, Age: {Age}, Grade: {Grade}";
    }
}